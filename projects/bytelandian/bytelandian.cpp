/*Program that solves the Bytelandian coin problem recursively*/
/* @TODO: Use dictionary to be able to exchange large Bytelandian coins */
#include <iostream>

int max = 1000000000;
long long n;
long long dict[max];

// Recursively check how to exchange
int exchange(int coin) {
	while (coin/2 + coin/3 + coin/4 > coin) {
		return std::max(coin, exchange(coin/2) + exchange(coin/3) + exchange(coin/4));
	}
	return coin;
}

/* Note that std::cin converts all input to integers */
int main() {
	std::cout << "Please enter initial integer on your coin \n";
	std::cout << "Bytelandians: \u0181 ";
	std::cin >> n;

	if (!cin.fail()) {
		long long change = exchange(n);
		std::cout << "Your maximum amount of currency equals: \u20B5 " << change << "\n";
		std::cout << "The profit of the exchange is: \u20B5 " << change - n << "\n";
	}
	else {
		std::cout << "Please enter an integer";
	}
	return 0;
}
